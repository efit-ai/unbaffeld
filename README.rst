UNBAFFELD: UNified Bayesian Analysis Framework for Fusion ExperimentaL Data
===========================================================================

Code, mostly in python, for performing Bayesian analysis of fusion data.

Testing code for creating pipeline for unbaffeld

Demos:
==========
`Fitting TS data with GPR <https://gitlab.com/efit-ai/unbaffeld/-/blob/main/unbaffeld/gpr/notebooks/TSFit.ipynb>`_

