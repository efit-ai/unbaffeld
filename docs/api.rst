.. _api:

API
===

.. module:: gpfit

This part of the documentation covers all the interfaces of unbaffeld.  


Gaussian Process Fit Object
---------------------------

.. autoclass:: GPTSFit
   :members:
   :inherited-members:

..
   autoclass:: HeteroskedasticUncertainties
   :members:
   :inherited-members:

