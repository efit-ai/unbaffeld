
unbaffeld
================

unbaffeld: UNified Bayesian Analysis Framework for Fusion ExperimentaL Data

*To Understand Baffling Fusion Data*

Welcome to unbaffeld's documentation.  unbaffeld is part of the 
`EFIT-AI`_ project.


User documentation
~~~~~~~~~~~~~~~~~~
.. toctree::
   :maxdepth: 2
   
   gpr
   quickstart
   api
   developer
   license
   install

   


.. _EFIT-AI: https://fusion.gat.com/conference/event/110

..
   include:: contents.rst.inc
